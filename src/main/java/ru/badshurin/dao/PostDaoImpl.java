package ru.badshurin.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.service.spi.ServiceException;
import ru.badshurin.utils.HibernateSessionFactory;

import java.util.ArrayList;
import java.util.List;

public class PostDaoImpl implements PostDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory=sessionFactory;
    }

    @Override
    public PostsEntity byName(String name) throws ServiceException {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Query query = session.createQuery("from PostsEntity where name = :paramName");
        query.setParameter("paramName", name);
        List list = query.list();
        PostsEntity post = null;
        try {
            post = (PostsEntity) list.get(0);
        }
        catch(IndexOutOfBoundsException e) {
            System.out.println("User not exist!");
        }
        if (session.isOpen()){
            session.close();
        }
        return post;
    }

    @Override
    public void addPost(PostsEntity user) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();

            session.save(user);
//            session.flush();
            tx1.commit();
        }
        catch (ServiceException e){
            System.out.println(e.fillInStackTrace());
        }
    }

    @Override
    public ArrayList<PostsEntity> findAllPost() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        ArrayList<PostsEntity> user = (ArrayList<PostsEntity>) session.createQuery("from PostsEntity").list();
        if (session.isOpen()){
            session.close();
        }
        return user;
    }

    @Override
    public PostsEntity findById(int id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Query query = session.createQuery("from PostsEntity where id = :paramName");
        query.setParameter("paramName", id);
        List list = query.list();
        PostsEntity post = null;
        try {
            post = (PostsEntity) list.get(0);
        }
        catch(IndexOutOfBoundsException e) {
            System.out.println("User not exist!");
        }
        if (session.isOpen()){
            session.close();
        }
        return post;
    }

    @Override
    public void deletePost(PostsEntity user) {
        try (Session session = HibernateSessionFactory.getSessionFactory().openSession()){
            Transaction tx1 = session.beginTransaction();

            session.delete(user);
//            session.flush();
            tx1.commit();
        }
        catch (ServiceException e){
            System.out.println(e.fillInStackTrace());
        }
    }


}
