package ru.badshurin.endpoint;

import ru.badshurin.ws.HelloWebServiceImpl;

import javax.xml.ws.Endpoint;

public class HelloWebServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:1988/wssalex/hellopeople", new HelloWebServiceImpl());
    }
}
