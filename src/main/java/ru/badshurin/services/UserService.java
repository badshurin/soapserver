package ru.badshurin.services;

import ru.badshurin.dao.PostsEntity;

import java.util.List;

public interface UserService {
    PostsEntity userByName(String name) throws Exception;
    void addUser(PostsEntity user);
    List<PostsEntity>  findAllUser();
}
