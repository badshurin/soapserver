package ru.badshurin.ws;

import ru.badshurin.dao.PostsEntity;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface HelloWebService {


    @WebMethod
    String getHelloString(String name);

    @WebMethod
    PostsEntity addPost(PostsEntity user);

    @WebMethod
    ArrayList<PostsEntity> findAllPost1();

    @WebMethod
    public PostsEntity getPostId(int postNo);

    @WebMethod
    public PostsEntity getPostName(String postName);

    @WebMethod
    void deletePostWS(PostsEntity user);

}
